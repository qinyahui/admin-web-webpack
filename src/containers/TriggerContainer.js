import {connect} from 'react-redux';
import {getTriggers, updateTriggers} from '../actions/Trigger';
import TriggerList from '../components/TriggerManagement/TriggerList';

const mapStateToProps = state => {
    return {
        Triggers:state.TriggerReducer,
    };
};

const mapDispatchToProps = {
    getTriggers,
    updateTriggers
}
const TriggerContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TriggerList);


export default TriggerContainer;