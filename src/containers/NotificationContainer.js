import {connect} from 'react-redux';
import {getNotifications, createNotification, updateNotification} from '../actions/Notifications';
import Notifications from '../components/Notifications/Notifications';

const mapStateToProps = state => {
    return {
        Notifications:state.NotificationReducer,

    };
};

const mapDispatchToProps = {
    getNotifications,
    createNotification,
    updateNotification
}
const NotificationContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Notifications);


export default NotificationContainer;