import axios from "axios";
import CONFIG from "../lib/config";

export default file => {
  let formData = new FormData();
  const fileName = `${Date.now()}_${file.name}`;

  formData.append("fileType", file.type);
  formData.append("fileName", fileName);
  formData.append("file", file);

  return axios
    .post(CONFIG.documentFetchUrl + "/upload", formData, {
      headers: {
        "Content-Type": "multipart/form-data"
      }
    })
    .then(res => {
      console.log("SUCCESS!!", res);
      return `${fileName}`;
    })
    .catch(function() {
      console.log("FAILURE!!");
    });
};
