import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    triggers: [],
    error: null,
    updatedData:[],
    isTriggerFetch: null
  };

  export default function TriggerReducer(state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_TRIGGER_STARTED:
        return {
          ...state,
          loading: true,
          isTriggerFetch: false
        };
      case types.GET_TRIGGER_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          triggers: action.payload,
          isTriggerFetch: true
        };
      case types.GET_TRIGGER_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isTriggerFetch: false
        };
        case types.UPDATE_TRIGGER_STARTED:
          return {
            ...state,
            loading: true
          };
        case types.UPDATE_TRIGGER_SUCCEEDED:
          return {
            ...state,
            loading: false,
            error: null,
            updatedData: action.payload
          };
        case types.UPDATE_TRIGGER_FAILED:
          return {
            ...state,
            loading: false,
            error: action.payload
          };
      default:
          return{
              ...state
          };
        }
    }