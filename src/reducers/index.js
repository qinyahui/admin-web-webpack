import { combineReducers } from "redux";
import UserReducer from './UserReducer';
import ProductReducer from './ProductReducer';
import TriggerReducer from './TriggerReducer';
import TriggerSourceReducer from './TriggerSourceReducer';
import CommonReducer from './CommonReducer';
import NotificationReducer from './NotificationReducer';
import FAQReducer from './FAQReducer';
import TandCReducer from './TandCReducer';
import SystemLogsReducer from './SystemLogsReducer';

const rootReducer = combineReducers({
    UserReducer,
    ProductReducer,
    TriggerReducer,
    TriggerSourceReducer,
    CommonReducer,
    NotificationReducer,
    FAQReducer,
    TandCReducer,
    SystemLogsReducer
});

export default rootReducer;