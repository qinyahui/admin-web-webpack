import * as types from '../lib/ActionTypes';

const initialState = {
    loading: false,
    TandC: [],
    error: null,
    isTandCFetch: null
  };

  export default function TandCReducer(state, action) {
    if(typeof state === "undefined"){
      return initialState;
    }
    switch (action.type) {
      case types.GET_TANDC_STARTED:
        return {
          ...state,
          loading: true,
          isTandCFetch: false
        };
      case types.GET_TANDC_SUCCEEDED:
        return {
          ...state,
          loading: false,
          error: null,
          TandC: action.payload,
          isTandCFetch: true
        };
      case types.GET_TANDC_FAILED:
        return {
          ...state,
          loading: false,
          error: action.payload,
          isTandCFetch: false
        };
        // case types.UPDATE_TRIGGER_STARTED:
        //   return {
        //     ...state,
        //     loading: true
        //   };
        // case types.UPDATE_TRIGGER_SUCCEEDED:
        //   return {
        //     ...state,
        //     loading: false,
        //     error: null,
        //     updatedData: action.payload
        //   };
        // case types.UPDATE_TRIGGER_FAILED:
        //   return {
        //     ...state,
        //     loading: false,
        //     error: action.payload
        //   };
      default:
          return{
              ...state
          };
        }
    }