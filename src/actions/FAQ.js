import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const getFAQStarted = data =>({
    type: types.GET_FAQ_STARTED,
    payload: data

});

const getFAQSuccess = data =>({
    type: types.GET_FAQ_SUCCEEDED,
    payload: data
});

const getFAQFailed = data =>({
    type: types.GET_FAQ_FAILED,
    payload: data
});

const getFAQCategoriesStarted = data =>({
  type: types.GET_FAQ_CATEGORIES_STARTED,
  payload: data

});

const getFAQCategoriesSuccess = data =>({
  type: types.GET_FAQ_CATEGORIES_SUCCEEDED,
  payload: data
});

const getFAQCategoriesFailed = data =>({
  type: types.GET_FAQ_CATEGORIES_FAILED,
  payload: data
});

const createFAQStarted = data =>({
  type: types.CREATE_FAQ_STARTED,
  payload: data

});

const createFAQSuccess = data =>({
  type: types.CREATE_FAQ_SUCCEEDED,
  payload: data
});

const createFAQFailed = data =>({
  type: types.CREATE_FAQ_FAILED,
  payload: data
});


const updateFAQStarted = data =>({
  type: types.UPDATE_FAQ_STARTED,
  payload: data

});

const updateFAQSuccess = data =>({
  type: types.UPDATE_FAQ_SUCCEEDED,
  payload: data
});

const updateFAQFailed = data =>({
  type: types.UPDATE_FAQ_FAILED,
  payload: data
});

export const getFAQs = (page, limit, searchText,token) => {
    return dispatch => {
      dispatch(getFAQStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/admin/getFAQ?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`, {headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(getFAQSuccess(res.data));
        })
        .catch(err => {
          dispatch(getFAQFailed(err.message));
        });
    };
  };
  export const getFAQCategories = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getFAQCategoriesStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/admin/getFAQCategory?pagenumber=${page}&pagesize=${limit}&searhtext=${searchText}`,{headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(getFAQCategoriesSuccess(res.data));
        })
        .catch(err => {
          dispatch(getFAQCategoriesFailed(err.message));
        });
    };
  };

  export const createFAQs = (params, token) => {
    return dispatch => {
      dispatch(createFAQStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/admin/upsertFAQ`, params, {headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(createFAQSuccess(res.data));
        })
        .catch(err => {
          dispatch(createFAQFailed(err.message));
        });
    };
  };

  export const updateFAQs = (params, token) => {
    return dispatch => {
      dispatch(updateFAQStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/admin/upsertFAQ`, params, {headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(updateFAQSuccess(res.data));
        })
        .catch(err => {
          dispatch(updateFAQFailed(err.message));
        });
    };
  };
