import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';
const getProductsStarted = data =>({
    type: types.GET_PRODUCT_STARTED,
    payload: data

});

const getProductsSuccess = data =>({
    type: types.GET_PRODUCT_SUCCEEDED,
    payload: data
});

const getProductsFailed = data =>({
    type: types.GET_PRODUCT_FAILED,
    payload: data
});

const updateProductsStarted = data =>({
  type: types.UPDATE_PRODUCT_STARTED,
  payload: data

});

const updateProductsSuccess = data =>({
  type: types.UPDATE_PRODUCT_SUCCEEDED,
  payload: data
});

const updateProductsFailed = data =>({
  type: types.UPDATE_PRODUCT_FAILED,
  payload: data
});

const updateGoodsTitleStarted = data =>({
  type: types.UPDATE_GOODS_TITLE_STARTED,
  payload: data

});

const updateGoodsTitleSuccess = data =>({
  type: types.UPDATE_GOODS_TITLE_SUCCEEDED,
  payload: data
});

const updateGoodsTitleFailed = data =>({
  type: types.UPDATE_GOODS_TITLE_FAILED,
  payload: data
});
export const getProducts = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getProductsStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/admin/getProduct?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`,{headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(getProductsSuccess(res.data));
        })
        .catch(err => {
          dispatch(getProductsFailed(err.message));
        });
    };
  };

export const updateProducts = (params, token) => {
    return dispatch => {
      dispatch(updateProductsStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/admin/updateProduct`, params,{headers:{
          authorization: CONFIG.token == null ? token : CONFIG.token
        }})
        .then(res => {
          dispatch(updateProductsSuccess(res.data));
        })
        .catch(err => {
          dispatch(updateProductsFailed(err.message));
        });
    };
  };

  export const updateGoodsTitle = (params, token) => {
    return dispatch => {
      dispatch(updateGoodsTitleStarted());
  
      axios
        .post(`${CONFIG.apiBaseUrl}/admin/updateProductTitle`, params,{headers:{
          authorization: token 
        }})
        .then(res => {
          dispatch(updateGoodsTitleSuccess(res.data));
        })
        .catch(err => {
          dispatch(updateGoodsTitleFailed(err.message));
        });
    };
  };