import * as types from '../lib/ActionTypes';
import CONFIG from '../lib/config';
import axios from 'axios';

const getNotificationsStarted = data =>({
    type: types.GET_NOTIFICATIONS_STARTED,
    payload: data

});

const getNotificationsSuccess = data =>({
    type: types.GET_NOTIFICATIONS_SUCCEEDED,
    payload: data
});

const getNotificationsFailed = data =>({
    type: types.CREATE_NOTIFICATIONS_FAILED,
    payload: data
});

const createNotificationsStarted = data =>({
  type: types.CREATE_NOTIFICATIONS_STARTED,
  payload: data

});

const createNotificationsSuccess = data =>({
  type: types.CREATE_NOTIFICATIONS_SUCCEEDED,
  payload: data
});

const createNotificationsFailed = data =>({
  type: types.GET_NOTIFICATIONS_FAILED,
  payload: data
});

const updateNotificationsStarted = data =>({
  type: types.UPDATE_NOTIFICATIONS_STARTED,
  payload: data

});

const updateNotificationsSuccess = data =>({
  type: types.UPDATE_NOTIFICATIONS_SUCCEEDED,
  payload: data
});

const updateNotificationsFailed = data =>({
  type: types.UPDATE_NOTIFICATIONS_FAILED,
  payload: data
});

export const getNotifications = (page, limit, searchText, token) => {
    return dispatch => {
      dispatch(getNotificationsStarted());
  
      axios
        .get(`${CONFIG.apiBaseUrl}/admin/getAnnouncement?pagenumber=${page}&pagesize=${limit}&searchtext=${searchText}`, {headers:{
          authorization: token
        }})
        .then(res => {
          dispatch(getNotificationsSuccess(res.data));
        })
        .catch(err => {
          dispatch(getNotificationsFailed(err.message));
        });
    };
  };

export const createNotification = (params,token) =>{
  return dispatch =>{
    dispatch(createNotificationsStarted());

    axios
       .post(`${CONFIG.apiBaseUrl}/admin/upsertAnnouncement`, params, {headers:{
        authorization: CONFIG.token == null ? token : CONFIG.token
      }})
       .then(res =>{
         dispatch(createNotificationsSuccess(res.data));
       })
       .catch(err =>{
         dispatch(createNotificationsFailed(err.message));
       });
  };
};

export const updateNotification = (params, token) =>{
  return dispatch =>{
    dispatch(updateNotificationsStarted());

    axios
       .post(`${CONFIG.apiBaseUrl}/admin/upsertAnnouncement`, params, {headers:{
        authorization: CONFIG.token == null ? token : CONFIG.token
      }})
       .then(res =>{
         dispatch(updateNotificationsSuccess(res.data));
       })
       .catch(err =>{
         dispatch(updateNotificationsFailed(err.message));
       });
  };
};