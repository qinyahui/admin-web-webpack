import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class Confirmation extends Component{

    toggle = () =>{
        this.props.delete();
    }
    cancel = () =>{
        this.props.cancel();
    }
    render(){
        return(
            <div>
                <Modal isOpen={this.props.isConfirm} className={this.props.className}>
                    <ModalHeader>Confirmation</ModalHeader>
                    <ModalBody>
                        Do you want to delete this notification?
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={e => this.toggle()}>Submit</Button>
                        <Button color="secondary" onClick={() => this.cancel()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
} 