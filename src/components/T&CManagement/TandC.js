import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Input, Button, Table } from 'reactstrap';
import Pagination from 'react-js-pagination';
import Confirmation from './Confirmation';
import CreateTandC from './CreateTandC';

export default class TandC extends Component {
    constructor(){
        super()
        this.state={
            isOpen: false,
            activePage: 1,
            searchText:"",
            isViewOpen: false,
            rowData: {},
            isConfirm: false
        }
    }

    componentDidMount (){
        this.props.getTandC(1,5,"", this.props.token);
    }

    delete =(row)=> {
        this.setState({
            isConfirm: true
        })
    }
    cancel =()=> {
        this.setState({
            isConfirm: false
        })
    }
    changeInput = (e) => {
        if (e.target.value.length > 3) {
            this.props.getTandC(this.state.activePage, 5, e.target.value)
        } else if(e.target.value.length === 0){
            this.props.getTandC(this.state.activePage, 5, "")
        }
        this.setState({
            searchText: e.target.value
        })
    }
    togglePopup =(row) =>{
        this.setState({
            isOpen: !this.state.isOpen,
            rowData: row
        })
    }
    render(){
        const columns = ['Header', 'Content', 'Action']
        const rows = this.props.TandC.TandC.rows
        const totalCount =  this.props.TandC.TandC.count;
        const totalPages = Math.floor(this.props.TandC.TandC.count)/5;

        return(
            <div>
                     <div>
                    <div style={{display: "flex", justifyContent: "space-between"}}>
                    <Input style={{ width: "200px", marginBottom: "10px" }} placeholder="Search..." onChange={(e) => this.changeInput(e)} value={this.state.searchText}></Input>
                  <div> <Button color="success" style={{marginBottom: "10px", marginRight:"10px"}} className="" onClick={()=>this.togglePopup()}>Create</Button>
                   <Button color="success" style={{marginBottom: "10px", marginRight:"10px"}} className="" onClick={()=>this.togglePopup()}>View</Button>
                   <Button color="success" style={{marginBottom: "10px"}} className="" onClick={()=>this.togglePopup()}>Edit</Button>
                   </div>
                   </div>
                    <Card>
                        <CardHeader>
                             Terms & Conditions
                    </CardHeader>
                        {rows !== undefined ?
                            <Table responsive striped>
                                <thead>
                                    <tr>
                                        {columns.map((item) =>
                                            <th>{item}</th>
                                        )} </tr>
                                </thead>
                                <tbody>{rows.length !== 0 ? rows.map((row) => <tr><td>{row.title}</td>
                                        <td>{row.content}</td>
                                        <td>
                                            <Button style={{marginRight: "10px"}} color="success" onClick={()=> this.delete(row)}>Delete</Button>
                                        </td>
    
                                    </tr>)
                                :<tbody>No data available.</tbody>}
                                </tbody></Table>
                            : ""}
                        <CardBody>
                            <Pagination
                                activePage={this.state.activePage}
                                itemsCountPerPage={5}
                                totalItemsCount={totalCount}
                                pageRangeDisplayed={totalPages}
                                itemClass="page-item"
                                linkClass="page-link"
                                onChange={(activePage) => this.handlePageChange(activePage)}
                            />
                        </CardBody>
                    </Card>
                    <Confirmation delete={this.delete} isConfirm={this.state.isConfirm} cancel={this.cancel} />
                    {this.state.isOpen && <CreateTandC isShow={this.state.isOpen} togglePopup={this.togglePopup} item={this.state.rowData}/>}
                    {/* {this.state.isViewOpen && <ViewNotifications isShow={this.state.isViewOpen} togglePopup={this.toggleView} item={this.state.rowData}/>}</div> */}
                </div >
            </div>
        )
    }
}