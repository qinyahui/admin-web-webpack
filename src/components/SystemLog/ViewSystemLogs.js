import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Container, Row, Col, Label } from 'reactstrap';


export default class ViewSystemLogs extends Component {
    constructor() {
        super()
        this.state = {
            notificationTitle: "",
            date: "",
            time: ""
        }
    }
    togglePopup = () => {
        this.props.togglePopup();
    }
    render() {
        const { item } = this.props;
        return (
            <div>
                <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
                    <ModalHeader toggle={() => this.togglePopup()}>System log detail</ModalHeader>
                    <ModalBody>
                        {/* {item !== undefined ? */}
                        <Container>
                            <Row>
                                <Col xs='4'><Label>User Name</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.userId}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Date and time of action</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.createdAt}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Action</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.message}</Label>
                                </Col>
                            </Row>
                        </Container>
                        {/* : ""} */}
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}
