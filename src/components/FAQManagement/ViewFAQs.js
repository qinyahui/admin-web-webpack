import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, Container, Row, Col, Label } from 'reactstrap';


export default class ViewFAQs extends Component {
    constructor() {
        super()
        this.state = {
            faqTitle: "",
            category: "",
            question: "",
            answer:""
        }
    }
    togglePopup = () => {
        this.props.toggleView();
    }
    render() {
        const { item } = this.props;
        return (
            <div>
                <Modal isOpen={this.props.isShow} toggle={() => this.togglePopup()} className={this.props.className}>
                    <ModalHeader toggle={() => this.togglePopup()}>FAQ</ModalHeader>
                    <ModalBody>
                        <Container>
                            <Row>
                                <Col xs='4'><Label>Category</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.categoryName}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Question</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.question !== null && item.question.replace(/(<([^>]+)>)/ig,"")}</Label>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs='4'><Label>Answer</Label></Col>
                                <Col xs='auto'>
                                    <Label>{item.answer !== null && item.question.replace(/(<([^>]+)>)/ig,"")}</Label>
                                </Col>
                            </Row>
                        </Container>
                        {/* : ""} */}
                    </ModalBody>
                </Modal>
            </div>
        )
    }
}
