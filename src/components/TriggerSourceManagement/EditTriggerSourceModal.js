import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Container, Input, Row, Col, Label } from 'reactstrap';
import axios from 'axios';
import CONFIG from '../../lib/config';

export default class EditTriggerSourceModal extends Component {
    constructor() {
        super()
        this.state = {
            active: "",
            description: "",
            triggerSourceName: "",
            flag: false,
            imagePreviewUrl: "",
            id: 1
        }
    }
    static getDerivedStateFromProps(props, state) {
        if (!state.flag && props.item !== undefined) {
            return {
                description: props.item.remarks,
                triggerSourceName: props.item.dataSourceName,
                active: props.item.status === 1 ? "Active" : "Inactive",
                imagePreviewUrl: `${props.item.imageUrl}`,
                id: props.item.id

            }
        } else {
            return {
                description: state.description,
                triggerSourceName: state.triggerSourceName,
                flag: false,
                imagePreviewUrl: state.imagePreviewUrl
            }
        }
    }
    changeInputText = (e) => {
        const name = e.target.name;
        this.setState({
            [name]: e.target.value,
            flag: true
        });
    }
    toggle = () => {
        this.props.togglePopup();
    }
    updateTriggerSource = () => {
        const data = {
            id: this.state.id,
            remarks: this.state.description,
            dataSourceName: this.state.triggerSourceName,
            // status: this.state.active === "yes" ? 1 : 0,
        }
        this.props.updateTriggerSource(data);
    }
    check = (e) => {
        this.setState({
            active: e.target.value,
            flag: true
        })
    }
    fileChange(e, fileName) {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        reader.onloadend = () => {
           this.setState({
               imagePreviewUrl: reader.result,
               flag: true
           })
        }
        if (fileName.match(/^\d/)) {
            let filename = fileName.slice(14, fileName.length);
            fileName = `${Date.now()}_${filename}`;
         } else{
            fileName = `${Date.now()}_${fileName}`;
         }
         const data = {
            id: this.state.id,
         imageUrl: fileName
         }
        reader.readAsDataURL(file);
        let formData = new FormData();
        formData.append('fileType', 'photo');
        formData.append('fileName', fileName);
        formData.append('file', file);
        axios.post(CONFIG.documentFetchUrl+'/upload',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        ).then(()=> {
            console.log('SUCCESS!!');
            this.props.updateTriggerSource(data);
        })
            .catch(function () {
                console.log('FAILURE!!');
            });
    }
    render() {
        const { item } = this.props;
        let { imagePreviewUrl } = this.state;
        return (
            <div>
                <Modal isOpen={this.props.isShow} toggle={() => this.toggle()} className={this.props.className}>
                    <ModalHeader toggle={() => this.toggle()}>Edit Trigger Source</ModalHeader>
                    <ModalBody>
                        {item !== undefined ?
                            <Container>
                                <Row>
                                    <Col xs='4'><Label>Status</Label></Col>
                                    <Col xs='auto'>
                                        <Label check>
                                        {this.state.active}
                                     </Label>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs='4'><Label>Trigger Source Name</Label></Col>
                                    <Col xs='auto'>
                                        <Input
                                            onChange={(e) => this.changeInputText(e)}
                                            name="triggerSourceName"
                                            value={this.state.triggerSourceName}>
                                        </Input>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs='4'><Label>Description</Label></Col>
                                    <Col xs='auto'>
                                        <Input
                                            onChange={(e) => this.changeInputText(e)}
                                            name="description"
                                            type="textarea"
                                            style={{ height: '100px' }}
                                            value={this.state.description}>
                                        </Input>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="4">Image</Col>
                                    <Col>
                                    <img src={imagePreviewUrl} type="file" className="portfoilio-img" onClick={() => document.getElementById('portfolio_img').click()}></img>
                                        <Input id="portfolio_img" type="file" style={{ display: "none" }} onChange={(e) => this.fileChange(e, imagePreviewUrl.slice(60, 85))}></Input>
                                    </Col>
                                </Row>
                            </Container> : ""}
                    </ModalBody>
                    <ModalFooter>
                        <Button color="success" onClick={() => this.updateTriggerSource()}>Update</Button>{' '}
                        <Button color="secondary" onClick={() => this.toggle()}>Cancel</Button>
                    </ModalFooter>
                </Modal>
            </div >
        )
    }
}