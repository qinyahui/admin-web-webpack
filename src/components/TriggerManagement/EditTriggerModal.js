import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Input,
  Row,
  Col,
  Label
} from "reactstrap";
import axios from "axios";
import { Select } from "antd";
import CONFIG from "../../lib/config";

const { Option } = Select;

export default class EditTriggerModal extends Component {
  constructor() {
    super();
    this.state = {
      active: "",
      description: "",
      triggerName: "",
      flag: false,
      imagePreviewUrl: "",
      id: 1
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (!state.flag && props.item !== undefined) {
      return {
        description: props.item.remarks,
        triggerName: props.item.triggerCategoryName,
        active: props.item.status === 1 ? "Active" : "Inactive",
        imagePreviewUrl: props.fileUrl,
        id: props.item.id,
        dailogImageUrl: `${props.item.dailogImageUrl}`,
        wheelNotSelectedImageUrl: `${props.item.wheelNotSelectedImageUrl}`,
        wheelSelectedImageUrl: `${props.item.wheelSelectedImageUrl}`,
        wheelSelectedAndTickedImageUrl: `${props.item.wheelSelectedAndTickedImageUrl}`,
        portofolioStackImageUrl: `${props.item.portofolioStackImageUrl}`,
        stackImageUrl: `${props.item.stackImageUrl}`
      };
    } else {
      return {
        description: state.description,
        triggerName: state.triggerName,
        active: state.active,
        flag: false,
        imagePreviewUrl: state.imagePreviewUrl
      };
    }
  }

  changeInputText = e => {
    const name = e.target.name;
    this.setState({
      [name]: e.target.value,
      flag: true
    });
  };

  toggle = () => {
    this.props.togglePopup();
  };

  updateTrigger = () => {
    const data = {
      id: this.state.id,
      remarks: this.state.description,
      triggerCategoryName: this.state.triggerName,
      status: this.state.active === "Active" ? 1 : 0
    };
    this.props.updateTrigger(data);
  };

  fileChange(e, fileName, fileUrl) {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        [fileUrl]: reader.result,
        flag: true
      });
    };
    if (fileName.match(/^\d/)) {
      let filename = fileName.slice(14, fileName.length);
      fileName = `${Date.now()}_${filename}`;
    } else {
      fileName = `${Date.now()}_${fileName}`;
    }
    reader.readAsDataURL(file);
    let formData = new FormData();
    formData.append("fileType", "photo");
    formData.append("fileName", fileName);
    formData.append("file", file);
    axios
      .post(CONFIG.documentFetchUrl + "/upload", formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(() => {
        // console.log('SUCCESS!!');
        const data = {
          id: this.state.id,
          [fileUrl]: fileName
        };
        this.props.updateTrigger(data);
      })
      .catch(function() {
        console.log("FAILURE!!");
      });
  }

  handleChange = value => {
    this.setState({
      active: value,
      flag: true
    });
  };

  render() {
    const { item } = this.props;
    // let { imagePreviewUrl } = this.state;
    return (
      <div>
        <Modal
          isOpen={this.props.isShow}
          toggle={() => this.toggle()}
          className={this.props.className}
        >
          <ModalHeader toggle={() => this.toggle()}>Edit Trigger</ModalHeader>
          <ModalBody>
            {item !== undefined ? (
              <Container>
                <Row>
                  <Col xs="4">
                    <Label>Status</Label>
                  </Col>
                  <Col xs="auto">
                    <Select
                      defaultValue={this.state.active}
                      onChange={this.handleChange}
                    >
                      <Option value="Inactive">Inactive</Option>
                      <Option value="Active">Active</Option>
                    </Select>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">
                    <Label>Trigger Name</Label>
                  </Col>
                  <Col xs="auto">
                    <Input
                      onChange={e => this.changeInputText(e)}
                      name="triggerName"
                      value={this.state.triggerName}
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">
                    <Label>Description</Label>
                  </Col>
                  <Col xs="auto">
                    <Input
                      onChange={e => this.changeInputText(e)}
                      name="description"
                      type="textarea"
                      style={{ height: "100px" }}
                      value={this.state.description}
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Dialog Image</Col>
                  <Col>
                    <img
                      src={this.state["dailogImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document.getElementById("dailogImageUrl").click()
                      }
                      alt=""
                    />
                    <Input
                      id="dailogImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["dailogImageUrl"].slice(60, 85),
                          "dailogImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Wheel not selected Image</Col>
                  <Col>
                    <img
                      src={this.state["wheelNotSelectedImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document
                          .getElementById("wheelNotSelectedImageUrl")
                          .click()
                      }
                      alt=""
                    />
                    <Input
                      id="wheelNotSelectedImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["wheelNotSelectedImageUrl"].slice(60, 81),
                          "wheelNotSelectedImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Wheel selected Image</Col>
                  <Col>
                    <img
                      src={this.state["wheelSelectedImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document.getElementById("wheelSelectedImageUrl").click()
                      }
                      alt=""
                    />
                    <Input
                      id="wheelSelectedImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["wheelSelectedImageUrl"].slice(60, 88),
                          "wheelSelectedImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Wheel selected & ticked Image</Col>
                  <Col>
                    <img
                      src={this.state["wheelSelectedAndTickedImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document
                          .getElementById("wheelSelectedAndTickedImageUrl")
                          .click()
                      }
                      alt=""
                    />
                    <Input
                      id="wheelSelectedAndTickedImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["wheelSelectedAndTickedImageUrl"].slice(
                            60,
                            95
                          ),
                          "wheelSelectedAndTickedImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Portfolio stack Image</Col>
                  <Col>
                    <img
                      src={this.state["portofolioStackImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document
                          .getElementById("portofolioStackImageUrl")
                          .click()
                      }
                      alt=""
                    />
                    <Input
                      id="portofolioStackImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["portofolioStackImageUrl"].slice(60, 81),
                          "portofolioStackImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
                <Row>
                  <Col xs="4">Stack Image</Col>
                  <Col>
                    <img
                      src={this.state["stackImageUrl"]}
                      type="file"
                      className="portfoilio-img"
                      onClick={() =>
                        document.getElementById("stackImageUrl").click()
                      }
                      alt=""
                    />
                    <Input
                      id="stackImageUrl"
                      type="file"
                      style={{ display: "none" }}
                      onChange={e =>
                        this.fileChange(
                          e,
                          this.state["stackImageUrl"].slice(60, 79),
                          "stackImageUrl"
                        )
                      }
                    ></Input>
                  </Col>
                </Row>
              </Container>
            ) : (
              ""
            )}
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={() => this.updateTrigger()}>
              Update
            </Button>{" "}
            <Button color="secondary" onClick={() => this.toggle()}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
