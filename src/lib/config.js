const CONFIG = {};

let apiBaseUrl, documentFetchUrl, verifyUrl;
const hostname = window && window.location && window.location.hostname;

if (hostname === "admin-web-snack.awsocp.sit.income.org.sg") {
  apiBaseUrl = "https://admin-snack.awsocp.sit.income.org.sg";
  documentFetchUrl = "https://upload-snack.awsocp.sit.income.org.sg";
  verifyUrl = "https://admin-snack.awsocp.sit.income.org.sg";
} else if (hostname === "admin-web-snack.uat.snackbyincome.sg") {
  apiBaseUrl = "https://admin-snack.awsocp.uat.income.org.sg";
  documentFetchUrl = "https://upload-snack.awsocp.uat.income.org.sg";
  verifyUrl = "https://admin-snack.awsocp.uat.income.org.sg";
} else if (hostname === "admin.snackbyincome.sg") {
  apiBaseUrl = "https://apigw.digitalincome.com.sg/snack/admin";
  documentFetchUrl = "https://apigw.digitalincome.com.sg/snack/upload";
  verifyUrl = "https://apigw.digitalincome.com.sg/snack/admin";
} else if (hostname === "admin-web-v5-snack.13.251.251.232.nip.io") {
  apiBaseUrl = "http://admin-snack.13.251.251.232.nip.io";
  documentFetchUrl = "http://upload-snack.13.251.251.232.nip.io";
  verifyUrl = "http://admin-snack.13.251.251.232.nip.io";
} else {
  apiBaseUrl =
    process.env.REACT_APP_API_BASE_URL ||
    "http://admin-snack.13.251.251.232.nip.io";
  documentFetchUrl =
    process.env.REACT_APP_DOCUMENT_FETCH_URL ||
    "http://upload-snack.13.251.251.232.nip.io";
  verifyUrl =
    process.env.REACT_APP_VERIFY_URL ||
    "http://admin-snack.13.251.251.232.nip.io";
}

CONFIG.apiBaseUrl = apiBaseUrl;
CONFIG.documentFetchUrl = documentFetchUrl;
CONFIG.verifyUrl = verifyUrl;
CONFIG.token = localStorage.getItem("token");

module.exports = CONFIG;